package com.jhonystein.pedidex.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "clientes")
@SequenceGenerator(name = "clientes_seq", allocationSize = 1)
public class Cliente {

    @Id
    @Column(name = "id_cliente")
    @GeneratedValue(generator = "clientes_seq", strategy = GenerationType.SEQUENCE)
    private Long id;
    @NotNull
    @Size(min = 11, max = 18)
    private String documento;
    @NotNull(message = "{Cliente.nome.NotNull}")
    @Size(min = 5, max = 80, message = "{Cliente.nome.Size}")
    private String nome;
    @Size(min = 4, max = 100)
    private String email;
    @Size(min = 8, max = 20)
    private String telefone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumento() {
        return documento;
    }

    public void setDocumento(String documento) {
        this.documento = documento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
    
    
}
